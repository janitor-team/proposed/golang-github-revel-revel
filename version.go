package revel

const (
	// Current Revel version
	VERSION    = "0.12.0dev"
	// Latest commit date
	BUILD_DATE = "2015-01-19"
	// Minimum required Go version
	MINIMUM_GO = ">= go1.3"
)
